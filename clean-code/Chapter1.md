# **CLEAN CODE**

* [Chapitre 1 : Clean Code](#chapitre-1-:-clean-code)
  - [There will be code](#there-will-be-code)
  - [Bad Code](#bad-code)
  - [What is clean code](#what-is-clean-code)
  - [School of thought](#school-of-thought)
  - [We are authors](#we-are-authors)
  - [The Boyscout rule](#the-boyscout-rule)
  - [Conclusion](#conclusion)

## **Chapitre 1 : Clean Code**

### **There will be code**
Y’aura toujours besoin de code et de développeur (c’est plutôt cool ça)

### **Bad Code**
Les « excuses » qui t’ont poussé à faire du bad code (Le temps, la fatigue, la pression, passer à autre chose) et les coûts du bad code sur le long terme (productivité qui tend vers 0).

### **What is clean code**
Pour résumé la parole des 6 personnages : C’est un code simple et efficace, qui est facile à lire par les autres. Qui contient des tests unitaires et d’acceptations. C’est un code dont on a pris soin.

### **School of thought**
C’est le fruit de plusieurs années d’expériences et d’échecs, il n’y a pas qu’une seule bonne façon de faire. Il y a une bonne façon de pratiquer le clean code selon leur manière de faire. Mais elle n’est pas la pratique absolue. Il est bien de lire ce qu’il se fait dans les autres écoles.

### **We are authors**
Nous sommes des auteurs, et nous avons donc des lecteurs. De ce fait, il faut faire l’effort de rendre facile et agréable la lecture du code

### **The boyscout rule**
Laisser le code dans un état plus propre que nous l’avions trouvé. Que ce soit un changement de nom de variable, de fonction, un « if » ...

### **Conclusion**
De la même manière qu’un livre d’art ne fera pas de toi un artiste, ce livre ne fera pas de toi un bon développeur. Mais il te donnera les outils, techniques, façons de penser utilisés. Il y aura du bon code, du mauvais code, du mauvais code transformé en bon code, des techniques, des disciplines ... et à toi de jouer!
